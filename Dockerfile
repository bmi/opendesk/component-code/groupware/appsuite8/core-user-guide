FROM registry.gitlab.open-xchange.com/frontend/dev_env/node_builder:latest AS builder

ARG APP_VERSION
ARG BUILD_TIMESTAMP
ARG CI_COMMIT_SHA

WORKDIR /app
COPY . /app
RUN yarn && npx grunt

FROM registry.gitlab.open-xchange.com/frontend/dev_env/distroless/nginx:latest
COPY --from=builder /app/build /usr/share/nginx/html